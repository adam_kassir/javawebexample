<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<header>
	<a href="${baseUrl }/admin">admin home</a>
	<a href="${baseUrl }/">retour au site</a>
	<a href="${baseUrl }/admin/categories">categories</a>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
	<c:if test = "${user == null}">
         <a href="${baseUrl }/connection">connection</a>
    </c:if>
	<c:if test = "${user != null}">
         ${user.name }
    </c:if>	
	
	
</header>