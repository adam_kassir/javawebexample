<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<header>
	<a href="${baseUrl }/">home</a>
	<a href="${baseUrl }/admin">administration</a>
	<a href="${baseUrl }/example">exemple</a>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
	<c:if test = "${user == null}">
         <a href="${baseUrl }/connection">connection</a>
    </c:if>
	<c:if test = "${user != null}">
         ${user.name }
    </c:if>	
	
	
</header>