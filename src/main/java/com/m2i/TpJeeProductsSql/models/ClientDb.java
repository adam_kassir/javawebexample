package com.m2i.TpJeeProductsSql.models;

import java.sql.Date;

public class ClientDb {
	private String email;
	private String nom;
	private String prenom;
	private Date dateDeNaissance;
	private String telephone;
	private String pseudo;
	private String entreprise;
	private String mdp;
	private int admin;
	private AdresseDb adresse;
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public Date getDateDeNaissance() {
		return dateDeNaissance;
	}
	public void setDateDeNaissance(Date dateDeNaissance) {
		this.dateDeNaissance = dateDeNaissance;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	public String getPseudo() {
		return pseudo;
	}
	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}
	public String getEntreprise() {
		return entreprise;
	}
	public void setEntreprise(String entreprise) {
		this.entreprise = entreprise;
	}
	public String getMdp() {
		return mdp;
	}
	public void setMdp(String mdp) {
		this.mdp = mdp;
	}
	public int getAdmin() {
		return admin;
	}
	public void setAdmin(int admin) {
		this.admin = admin;
	}
	public AdresseDb getAdresse() {
		return adresse;
	}
	public void setAdresse(AdresseDb adresse) {
		this.adresse = adresse;
	}
	@Override
	public String toString() {
		return "ClientDb [email=" + email + ", nom=" + nom + ", prenom=" + prenom + ", dateDeNaissance="
				+ dateDeNaissance + ", telephone=" + telephone + ", pseudo=" + pseudo + ", entreprise=" + entreprise
				+ ", mdp=" + mdp + ", admin=" + admin + ", adresse=" + adresse + "]";
	}
	
	//String query = "INSERT INTO `amazon_db`.`client` (`email`, `nom`, `prenom`, `date_de_naissance`, `telephone`, `pseudo`, `entreprise`, `mdp`, `admin`, `adresse_id`) VALUES (?, ?, ?, ?, ?, ?, ?,?, ?, ?);";
	
	
	
	
}
