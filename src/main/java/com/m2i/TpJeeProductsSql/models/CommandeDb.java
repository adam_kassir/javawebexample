package com.m2i.TpJeeProductsSql.models;

import java.sql.Date;

public class CommandeDb {
	private long id;
	private Date date;
	private ClientDb client;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public ClientDb getClient() {
		return client;
	}
	public void setClient(ClientDb client) {
		this.client = client;
	}
	@Override
	public String toString() {
		return "CommandeDb [id=" + id + ", date=" + date + ", client=" + client + "]";
	}
	
	
}
