package com.m2i.TpJeeProductsSql.models;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CategorieDb {
	private long id;
	private String nom;
	private long parent;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public long getParent() {
		return parent;
	}
	public void setParent(long parent) {
		this.parent = parent;
	}
	@Override
	public String toString() {
		return "CategorieDb [id=" + id + ", nom=" + nom + ", parent=" + parent + "]";
	}
	public CategorieDb fromSql(ResultSet rs) {
		
		try {
			this.setId(rs.getLong("id_cat"));
			this.setNom(rs.getString("nom"));
			this.setParent(rs.getLong("parent"));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return this;
	}

	
}
