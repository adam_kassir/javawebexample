package com.m2i.TpJeeProductsSql.models;

public class ProduitDb {
	private long idProduit;
	private String nom;
	private String image;
	private String description;
	private double prix;
	private int promo;
	private int stock;
	private CategorieDb categorie;
	public long getIdProduit() {
		return idProduit;
	}
	public void setIdProduit(long idProduit) {
		this.idProduit = idProduit;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public double getPrix() {
		return prix;
	}
	public void setPrix(double prix) {
		this.prix = prix;
	}
	public int getPromo() {
		return promo;
	}
	public void setPromo(int promo) {
		this.promo = promo;
	}
	public int getStock() {
		return stock;
	}
	public void setStock(int stock) {
		this.stock = stock;
	}
	public CategorieDb getCategorie() {
		return categorie;
	}
	public void setCategorie(CategorieDb categorie) {
		this.categorie = categorie;
	}
	@Override
	public String toString() {
		return "ProduitDb [idProduit=" + idProduit + ", nom=" + nom + ", image=" + image + ", description="
				+ description + ", prix=" + prix + ", promo=" + promo + ", stock=" + stock + ", categorie=" + categorie
				+ "]";
	}
	
	
}
