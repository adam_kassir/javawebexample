package com.m2i.TpJeeProductsSql.controllers;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.m2i.TpJeeProductsSql.database.queries.CategorieQueries;
import com.m2i.TpJeeProductsSql.models.CategorieDb;

/**
 * Servlet implementation class ExampleController
 */
@WebServlet(urlPatterns = {"/example"})
public class ExampleController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ExampleController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("example log");
//		Connector.init();
		// TODO Auto-generated method stub
		
//		ClassLoader loader = Thread.currentThread().getContextClassLoader();
//		Properties props = new Properties();
//		
//		try (InputStream inputStream  = loader.getResourceAsStream("config.properties"))
//		{
//			props.load(inputStream);
//		}
//		System.out.println(props.getProperty("jdbc.driver.class"));
//		
//		try {
//			Class.forName(props.getProperty("jdbc.driver.class"));
//		} catch (ClassNotFoundException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
//		String url = props.getProperty("jdbc.url");
//		String login = props.getProperty("jdbc.login");
//		String pass = props.getProperty("jdbc.password");
		
//		try {
//			Connection connection = DriverManager.getConnection(url,login,pass);

		
//		try (Connection connection = Connector.open())
//		{
//			
//			String query = "INSERT INTO `amazon_db`.`client` (`email`, `nom`, `prenom`, `date_de_naissance`, `telephone`, `pseudo`, `entreprise`, `mdp`, `admin`, `adresse_id`) VALUES (?, ?, ?, ?, ?, ?, ?,?, ?, ?);";
//			
//			
//			
//			//System.out.println(connection.getSchema());
//			PreparedStatement ps = connection.prepareStatement(query);
//			ps.setString(1, "dse@a.com");
//			ps.setString(2, "nom blabla");
//			ps.setString(3, "prenom blabla");
//			ps.setString(4,null );
//			ps.setString(5, "0345678910");
//			ps.setString(6, "pseudo bidule");
//			ps.setString(7, "sfr");
//			ps.setString(8, "toto");
//			ps.setInt(9, 1);
//			ps.setString(10, null);
//			
//			ps.executeUpdate();
//			
//			
//			
//		} catch (SQLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}

//		String searchQuery = "SELECT * FROM client";
//		try (Connection connection = Connector.open())
//		{		
//			try (Statement st = connection.createStatement() )
//			{
//				ResultSet rs;
//				try {
//					rs = st.executeQuery(searchQuery);
//				
//					while(rs.next()) {
//						System.out.println("email" + rs.getString("email"));
//					}
//				} catch (SQLException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//			}
//		} catch (SQLException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}	
		ServletContext context = request.getSession().getServletContext();
		CategorieQueries q =  new CategorieQueries();
		ArrayList<CategorieDb> categories = new CategorieQueries().findAll();
		context.setAttribute("categories",categories);
		CategorieDb test =new CategorieQueries().findById(1);
		System.out.println("recuperation de test from db id=1");
		System.out.println("objet");
		System.out.println(test);
		context.setAttribute("test",test);
		CategorieDb test2 = new CategorieDb();
		test2.setNom("nourriture12");
		test2.setParent(1);
		q.create(test2);
		CategorieDb test3 = new CategorieDb();
		test3.setNom("nourriture12");
		test3.setParent(test.getId());
		q.create(test3);
		System.out.println("update");
		test.setNom("nouveau nourriture yeah");

		System.out.println(test.getNom());
		q.update(test);
		CategorieDb test4 =new CategorieQueries().findById(4);
		if (test4 != null)
		{
		q.delete(test4);
		}
		//System.out.println("categories" + categories.get(0).toString());
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		request.setAttribute("baseUrl",request.getContextPath());
		response.setContentType("text/html;charset=UTF-8;");
		this.getServletContext().getRequestDispatcher("/WEB-INF/example.jsp").forward(request,response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
