package com.m2i.TpJeeProductsSql.controllers;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Properties;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.m2i.TpJeeProductsSql.database.Connector;
import com.m2i.TpJeeProductsSql.database.queries.CategorieQueries;
import com.m2i.TpJeeProductsSql.models.CategorieDb;

/**
 * Servlet implementation class IndexController
 */
@WebServlet(urlPatterns = {"/index",""})
public class IndexController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public IndexController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("index log");
		ServletContext context = request.getSession().getServletContext();
		CategorieQueries q =  new CategorieQueries();
		ArrayList<CategorieDb> categories = new CategorieQueries().findAll();
		context.setAttribute("categories",categories);
		request.setAttribute("baseUrl",request.getContextPath());
		response.setContentType("text/html;charset=UTF-8;");
		this.getServletContext().getRequestDispatcher("/WEB-INF/index.jsp").forward(request,response);
		
	}
	
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
