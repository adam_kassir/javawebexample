package com.m2i.TpJeeProductsSql.adminControllers;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.m2i.TpJeeProductsSql.database.queries.CategorieQueries;
import com.m2i.TpJeeProductsSql.models.CategorieDb;

/**
 * Servlet implementation class AdminCategoryController
 */

@WebServlet(urlPatterns = {"/admin/categories"})
public class AdminCategoryController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdminCategoryController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ServletContext context = request.getSession().getServletContext();
		//CategorieQueries q =  new CategorieQueries();
		ArrayList<CategorieDb> categories = new CategorieQueries().findAll();
		
		request.setAttribute("categories",categories);		
		request.setAttribute("baseUrl",request.getContextPath());
		response.setContentType("text/html;charset=UTF-8;");
		this.getServletContext().getRequestDispatcher("/WEB-INF/admin/categories.jsp").forward(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
