package com.m2i.TpJeeProductsSql.database.queries;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.m2i.TpJeeProductsSql.database.Connector;
import com.m2i.TpJeeProductsSql.models.CategorieDb;

public class CategorieQueries {

	
	public ArrayList<CategorieDb> findAll()
	{
		ArrayList<CategorieDb> categories = new ArrayList<CategorieDb>();
		String searchQuery = "SELECT * FROM categorie";
		try (Connection connection = Connector.open())
		{		
			try (Statement st = connection.createStatement() )
			{
				ResultSet rs;
				try {
					rs = st.executeQuery(searchQuery);			
					while(rs.next()) {
						CategorieDb cat = new CategorieDb();
						
						categories.add(cat.fromSql(rs));
						//System.out.println("email" + rs.getString("email"));
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		return categories;
		
	}
	
	public CategorieDb findById(long inId)
	{
		CategorieDb cat = new CategorieDb();
		String searchQuery = String.format("SELECT * FROM categorie WHERE id_cat = %d",inId )  ;
		try (Connection connection = Connector.open())
		{		
			try (Statement st = connection.createStatement() )
			{
				ResultSet rs;
				try {
					rs = st.executeQuery(searchQuery);			
					while(rs.next()) {					
						cat.fromSql(rs);
						return cat;
						//System.out.println("email" + rs.getString("email"));
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		return null;
		
	}
	
	public void create(CategorieDb cat)
	{
		
		//String sqlQuery = "INSERT INTO `amazon_db`.`categorie` (`nom`) VALUES ('bidules trucs');";
		//String sqlQuery = "INSERT INTO `amazon_db`.`categorie` (`nom`) VALUES ('" +cat.getNom() +"');" ;
		String sqlQuery = "INSERT INTO `amazon_db`.`categorie` (`nom`,`parent`) VALUES (?,?);" ;
		//String sqlQuery = "INSERT INTO `amazon_db`.`client` (`email`, `nom`, `prenom`, `date_de_naissance`, `telephone`, `pseudo`, `entreprise`, `mdp`, `admin`, `adresse_id`) VALUES (?, ?, ?, ?, ?, ?, ?,?, ?, ?);";
				
		try (Connection connection = Connector.open())
		{		
			try (PreparedStatement st = connection.prepareStatement(sqlQuery) )
			{
				System.out.println("nvo nom " + cat.getNom());
				st.setString(1, cat.getNom());
				String lo = String.valueOf( cat.getParent());
				Integer i = Integer.parseInt(lo);
				//System.out.println("nvo parent" + cat.getParent().getId());
				st.setInt(2,i);
				st.executeUpdate();	
				//st.close();
			}
		} catch (SQLException e1) {
			e1.printStackTrace();
		}		
	}
	public void update(CategorieDb cat)
	{
		
		//String sqlQuery = "INSERT INTO `amazon_db`.`categorie` (`nom`) VALUES ('bidules trucs');";
		//String sqlQuery = "INSERT INTO `amazon_db`.`categorie` (`nom`) VALUES ('" +cat.getNom() +"');" ;
		String sqlQuery = "UPDATE `amazon_db`.`categorie` SET `nom` = ?, `parent` = ? WHERE (`id_cat` = ?);";
		//String sqlQuery = "INSERT INTO `amazon_db`.`categorie` (`nom`) VALUES (?);" ;
		//String sqlQuery = "INSERT INTO `amazon_db`.`client` (`email`, `nom`, `prenom`, `date_de_naissance`, `telephone`, `pseudo`, `entreprise`, `mdp`, `admin`, `adresse_id`) VALUES (?, ?, ?, ?, ?, ?, ?,?, ?, ?);";
				
		try (Connection connection = Connector.open())
		{		
			try (PreparedStatement st = connection.prepareStatement(sqlQuery) )
			{
				System.out.println("update nom " + cat.getNom());
				st.setString(1, cat.getNom());
				String lo = String.valueOf( cat.getParent());
				Integer i = Integer.parseInt(lo);
				System.out.println(" parent " + cat.getParent());
				st.setInt(2,i);
				String lo2 = String.valueOf( cat.getId());
				Integer j = Integer.parseInt(lo2);
				System.out.println("for id" + cat.getParent());
				st.setInt(3,j);
				//System.out.println("nvo parent" + cat.getParent().getId());
				//st.setInt(2,cat.getParent().getId());
				st.executeUpdate();	
				//st.close();
			}
		} catch (SQLException e1) {
			e1.printStackTrace();
		}		
	}
	public void delete(CategorieDb cat)
	{
		
		String sqlQuery = "DELETE FROM `amazon_db`.`categorie` WHERE (`id_cat` = ?);";		
		try (Connection connection = Connector.open())
		{		
			try (PreparedStatement st = connection.prepareStatement(sqlQuery) )
			{
				System.out.println("delete");
				String lo2 = String.valueOf( cat.getId());
				Integer j = Integer.parseInt(lo2);
				System.out.println("for id" + j);
				st.setInt(1,j);
				//System.out.println("nvo parent" + cat.getParent().getId());
				//st.setInt(2,cat.getParent().getId());
				st.executeUpdate();	
				//st.close();
			}
		} catch (SQLException e1) {
			e1.printStackTrace();
		}		
	}	
}
