package com.m2i.TpJeeProductsSql.database;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class Connector {

	private static String driver;
	private static String url;
	private static String login;
	private static String pass;
	


	@SuppressWarnings("rawtypes")
	public static void init() {
		System.out.println("initialisation de la connexion a la db");
		ClassLoader loader = Thread.currentThread().getContextClassLoader();
		Properties props = new Properties();
		
		try (InputStream inputStream  = loader.getResourceAsStream("config.properties"))
		{
			props.load(inputStream);

			System.out.println(props.getProperty("jdbc.driver.class"));
			
			try {
				driver = props.getProperty("jdbc.driver.class");
				Class driver_class = Class.forName(driver);
	            Driver driverObj = (Driver) driver_class.newInstance();
	            DriverManager.registerDriver(driverObj);
				
				Class.forName(props.getProperty("jdbc.driver.class"));
				url = props.getProperty("jdbc.url");
				login = props.getProperty("jdbc.login");
				pass = props.getProperty("jdbc.password");
				System.out.println("connection a la db reussie");
			} catch (ClassNotFoundException e) {
	            e.printStackTrace();
	        } catch (IllegalAccessException e) {
	            e.printStackTrace();
	        } catch (InstantiationException e) {
	            e.printStackTrace();
	        } catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public static Connection open() throws SQLException
	{
		Connection connection = DriverManager.getConnection(url,login,pass);
		return connection;
	}
}
